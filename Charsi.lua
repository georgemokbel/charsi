local _, ns = ... 
Charsi = Charsi or { }
Charsi.ns = ns
_G["Charsi"] = Charsi.ns

function PrintStatus()
  if charsisv.Enabled then
    print("Charsi is enabled.")
  else
    print("Charsi is disabled.")
  end
end


SLASH_CHARSI_CHARSI1 = "/charsi"
SlashCmdList["CHARSI_CHARSI"] = function(msg, editbox)
  charsisv.Enabled = not charsisv.Enabled
  PrintStatus()
end

function Charsi_OnEvent(event)
  if event == "ADDON_LOADED" then
    if not charsisv then
      charsisv = {}
      charsisv.Enabled = true
    end
    CharsiFrame:UnregisterEvent("ADDON_LOADED")
  elseif event == "MERCHANT_SHOW" then
    if charsisv.Enabled then
      SellJunk()
    end
  elseif event == "PLAYER_LOGIN" then
    if charsisv.Enabled then
      print("Charsi is loaded and enabled.")
    else
      print("Charsi is loaded but disabled.")
    end
  end
end

function SellJunk()
  local totalValueOfVendoredItems = 0
  local numberOfUniqueItemsVendored = 0
  local numberOfItemsVendored = 0
  for i = 0, 4, 1 do
      local containerSlots = GetContainerNumSlots(i)
      for j = 1, containerSlots, 1 do
          local itemLink = GetContainerItemLink(i,j)
          if itemLink then
              local _,_,quality,_,_,_,_,_,_,_,vendorValue = GetItemInfo(itemLink)
              if quality == 0 then
                  local count = GetItemCount(itemLink)
                  numberOfUniqueItemsVendored = numberOfUniqueItemsVendored + 1
                  numberOfItemsVendored = numberOfItemsVendored + count
                  totalValueOfVendoredItems = totalValueOfVendoredItems + (count * vendorValue)
                  PickupContainerItem(i,j)
                  if CursorHasItem() then
                      ShowContainerSellCursor(i, j)
                      UseContainerItem(i, j)
                      ClearCursor()
                  end
              end             
          end          
      end     
  end
  if numberOfUniqueItemsVendored > 0 then 
    totalValueAsString = ParseTotalValueOfVendoredItems(totalValueOfVendoredItems)
    print(string.format("Sold %u (%u unique) items for %s", numberOfItemsVendored, numberOfUniqueItemsVendored, totalValueAsString))
  end
end

function ParseTotalValueOfVendoredItems(value)
  local gold = math.floor(value / 10000) -- 10,000 copper per gold piece.
  value = value % 10000
  local silver = math.floor(value / 100) -- 100 copper per silver piece
  value = value % 100
  local copper = value
  return string.format("%u gold, %u silver and %u copper.", gold, silver, copper)
end
